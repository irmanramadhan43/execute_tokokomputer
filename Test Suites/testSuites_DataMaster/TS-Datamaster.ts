<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS-Datamaster</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>70528e69-d70c-47af-b81d-02fef454a9ec</testSuiteGuid>
   <testCaseLink>
      <guid>35a6bb6a-ee45-4bc5-8a77-00f456be841c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/0_dasboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d8f7ce2-4144-42c8-9c50-9ccd06e02c22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/1_tambahSupplier</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a6c90e8-b492-4669-9802-6fd590a6d1ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/2_tambahKategori</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdd5e943-c5e0-4cd9-92ad-ee85869058d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/3_tambahSubKategori</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59666069-d355-4f80-9885-5bdd12237aa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/4_tambahSuperSubKategori</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcde6a81-b43d-453f-b10f-c32657e8ea21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ADMIN/Data Master/5_tambahProduk</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
