package com.helper.browserhelper

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.junit.After

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable


public class customBrowser {

	@Keyword
	public void login(){

		WebUI.openBrowser("http://localhost/kl_skripsi/admin/auth/login")

		WebUI.setText(findTestObject('Object Repository/1_Dasboard_OR/input_Admin_email'), 'admin@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/1_Dasboard_OR/input_Admin_password'), 'RAIVpflpDOg=')

		WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/button_Sign In'))

		WebUI.verifyElementVisible(findTestObject('Object Repository/1_Dasboard_OR/img_Toggle navigation_user-image'))
	}

	@Keyword
	public void openBrowserParamemitrized(String url){

		WebUI.openBrowser(url)

		WebUI.setText(findTestObject('Object Repository/1_Dasboard_OR/input_Admin_email'), 'admin@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/1_Dasboard_OR/input_Admin_password'), 'RAIVpflpDOg=')

		WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/button_Sign In'))
	}

	@Keyword
	public void closeBrowser(){
		/*
		 * 1. Check For The Frame
		 * 2. Check for the parent Browser
		 * 3. Close The Browser
		 * */




		WebUI.closeBrowser()
	}

	@Keyword
	public void logout(){

		WebUI.waitForElementVisible(findTestObject('Object Repository/1_Dasboard_OR/span_Halo admin'), 2, FailureHandling.STOP_ON_FAILURE)

		WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Halo admin'))

		WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/Logout'),FailureHandling.STOP_ON_FAILURE)

		WebUI.delay(GlobalVariable.delayTime)
	}
}
