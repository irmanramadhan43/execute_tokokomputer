<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_DATA MASTER_fa fa-angle-left pull-right</name>
   <tag></tag>
   <elementGuidId>05b328d2-2e5a-426d-a4bc-872711b8b296</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//i[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[@class=&quot;skin-green sidebar-mini&quot;]/aside[@class=&quot;main-sidebar&quot;]/section[@class=&quot;sidebar&quot;]/ul[@class=&quot;sidebar-menu&quot;]/li[4]/a[1]/i[@class=&quot;fa fa-angle-left pull-right&quot;][count(. | //*[@class = 'fa fa-angle-left pull-right']) = count(//*[@class = 'fa fa-angle-left pull-right'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-angle-left pull-right</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;skin-green sidebar-mini&quot;]/aside[@class=&quot;main-sidebar&quot;]/section[@class=&quot;sidebar&quot;]/ul[@class=&quot;sidebar-menu&quot;]/li[4]/a[1]/i[@class=&quot;fa fa-angle-left pull-right&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//i[2]</value>
   </webElementXpaths>
</WebElementEntity>
