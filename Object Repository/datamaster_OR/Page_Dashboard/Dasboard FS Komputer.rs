<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dasboard FS Komputer</name>
   <tag></tag>
   <elementGuidId>3ad5636a-adcc-4561-bb06-0da28776f624</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body/div[@class='wrapper']/header/a[@href='https://fskomputer.store/admin/dashboard']//b[.='FS KOMPUTER']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'wrapper']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wrapper</value>
   </webElementProperties>
</WebElementEntity>
