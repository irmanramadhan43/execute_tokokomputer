<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_user_name</name>
   <tag></tag>
   <elementGuidId>2d4dad6e-82b7-44d5-8e91-eb581f4c0514</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//flt-glass-pane/input[@class='flt-text-editing']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//flt-glass-pane/input[@class='flt-text-editing']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//flt-glass-pane/input[@class='flt-text-editing']</value>
   </webElementProperties>
</WebElementEntity>
