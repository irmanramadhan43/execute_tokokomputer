<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>personalFolder_1</name>
   <tag></tag>
   <elementGuidId>1e4a63ee-d251-4bc6-bd14-3a994c145e00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='example']//div[@class='RadAjaxPanel']/div[@class='Panel1']/div[1]/div[@class='RadTreeView RadTreeView_Black']/ul/li/div/div[@class='rtIn']/span[@class='rtText']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>https://demos.telerik.com/aspnet-ajax/treeview/examples/functionality/draganddropnodes/defaultcs.aspx?skin=Black</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='example']//div[@class='RadAjaxPanel']/div[@class='Panel1']/div[1]/div[@class='RadTreeView RadTreeView_Black']/ul/li/div/div[@class='rtIn']/span[@class='rtText']</value>
   </webElementProperties>
</WebElementEntity>
