<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>searchTextBox</name>
   <tag></tag>
   <elementGuidId>8358c2c4-997d-4215-9c84-ce27adf836c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='skin-chooser']//div[@class='RadSearchBox RadSearchBox_Qsf']//input[@name='ctl00_DemoSearch1_SearchBox']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='skin-chooser']//div[@class='RadSearchBox RadSearchBox_Qsf']//input[@name='ctl00_DemoSearch1_SearchBox']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='skin-chooser']//div[@class='RadSearchBox RadSearchBox_Qsf']//input[@name='ctl00_DemoSearch1_SearchBox']</value>
   </webElementProperties>
</WebElementEntity>
