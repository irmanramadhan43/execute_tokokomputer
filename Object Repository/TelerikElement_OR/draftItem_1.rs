<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>draftItem_1</name>
   <tag></tag>
   <elementGuidId>226d81c4-3461-41ec-96b9-2fef04e39f83</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@id='example']//div[@class='RadAjaxPanel']/div[@class='Panel1']/div[1]/div[@class='RadTreeView RadTreeView_Black']/ul/li/ul[@class='rtUL']/li[2]/div/div[@class='rtIn']/span[@class='rtText']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example']//div[@class='RadAjaxPanel']/div[@class='Panel1']/div[1]/div[@class='RadTreeView RadTreeView_Black']/ul/li/ul[@class='rtUL']/li[2]/div/div[@class='rtIn']/span[@class='rtText']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@id='example']//div[@class='RadAjaxPanel']/div[@class='Panel1']/div[1]/div[@class='RadTreeView RadTreeView_Black']/ul/li/ul[@class='rtUL']/li[2]/div/div[@class='rtIn']/span[@class='rtText']</value>
   </webElementProperties>
</WebElementEntity>
