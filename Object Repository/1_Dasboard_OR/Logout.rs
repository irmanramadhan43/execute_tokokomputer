<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Logout</name>
   <tag></tag>
   <elementGuidId>5e08e893-e1da-4aca-9469-53fb52230800</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body/header[@class='main-header']/nav[@role='navigation']//ul[@class='nav navbar-nav']//ul[@class='dropdown-menu']//a[@href='https://localhost/kl_skripsi/admin/auth/logout']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>.dropdown-toggle > .hidden-xs</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//body/header[@class='main-header']/nav[@role='navigation']//ul[@class='nav navbar-nav']//ul[@class='dropdown-menu']//a[@href='https://localhost/kl_skripsi/admin/auth/logout']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.dropdown-toggle > .hidden-xs</value>
   </webElementProperties>
</WebElementEntity>
