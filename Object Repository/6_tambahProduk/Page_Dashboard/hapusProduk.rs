<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hapusProduk</name>
   <tag></tag>
   <elementGuidId>4cf92fdf-6806-4656-bf5a-81636ce78566</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='datatable']/tbody/tr[1]//a[@title='Hapus']/i[@class='fa fa-remove']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//table[@id='datatable']/tbody/tr[1]//a[@title='Hapus']/i[@class='fa fa-remove']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[@id='datatable']/tbody/tr[1]//a[@title='Hapus']/i[@class='fa fa-remove']</value>
   </webElementProperties>
</WebElementEntity>
