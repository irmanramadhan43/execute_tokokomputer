import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/span_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/span_Data Produk'))

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/a_Tambah Produk'))

TestData excelData = findTestData('Data Files/ExcelDataSource/Data Produk Prosesor')

System.out.println('[Row Count] : ' + excelData.getRowNumbers())

for (i = 1; i <= excelData.getRowNumbers(); i++) {
	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Nama Produk_judul_produk'),
			findTestData('ExcelDataSource/Data Produk Prosesor').getValue('Nama Produk', i))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Keywords_keywords'), findTestData(
			'ExcelDataSource/Data Produk Prosesor').getValue('Keyword', i))

	WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/p'))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/body_8 core 16 thread'), findTestData(
			'ExcelDataSource/Data Produk Prosesor').getValue('Deskripsi ', i))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Harga Normal_harga_normal'),
			findTestData('ExcelDataSource/Data Produk Prosesor').getValue('Harga Normal', i))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Diskon ()_diskon'), findTestData(
			'ExcelDataSource/Data Produk Prosesor').getValue('Diskon', i))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Stok_stok'), findTestData(
			'ExcelDataSource/Data Produk Prosesor').getValue('Stok', i))

	WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Berat_berat'), findTestData(
			'ExcelDataSource/Data Produk Prosesor').getValue('Berat', i))

	WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_- Pilih Kategori -ProsesorRamVgaStro_643f17'),
			'3', true)

	WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/div_Stok'))

	WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_- Pilih Kategori -ProsesorRamVgaStro_643f17'),
			'3', true)

	WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_AmdI9 SeriesIntel'),
			'8', true)

	WebUI.selectOptionByValue(findTestObject('6_tambahProduk/Page_Tambah Data Produk/select_I9 Series'), '12', true)

	WebUI.uploadFile(findTestObject('6_tambahProduk/Page_Dashboard/uploadGambar'), 'C:\\Users\\User\\Pictures\\426f99bfecf0f226ee043a719a4e6f4f.jpg')

	WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/button_Simpan'))

	WebUI.click(findTestObject('6_tambahProduk/Page_Data Produk/tambahData'))
}

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)


