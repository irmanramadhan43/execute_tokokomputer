import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demos.telerik.com/aspnet-ajax/datepicker/overview/defaultcs.aspx')

WebUI.setText(findTestObject('Object Repository/TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/input_From Date_ctl00ContentPlaceholder1Rad_38d06f'), 
    '12/04/1994')

WebUI.click(findTestObject('Object Repository/TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/a_From Date_ctl00_ContentPlaceholder1_RadDa_152b9d'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/a_From Date_ctl00_ContentPlaceholder1_RadDa_152b9d'))

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_arrow_left'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_arrow_double_left'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_arrow_right'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_arrow_double_right'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/pop_up_month_years'))

WebUI.delay(2)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_month_years_Prev'))

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_month_years_Prev'))

WebUI.delay(2)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_month_years_next'))

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_month_years_next'))

WebUI.delay(2)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUP_pilih_tahun'))

WebUI.delay(2)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/PopUp_pilih_bulan'))

WebUI.delay(2)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_OK'))

WebUI.delay(5)

WebUI.click(findTestObject('TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/popUp_pilih_tanggal'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/TelerikElement_OR/Page_AJAX DatePicker Demo  RadDatePicker fo_590143/span_Search'))

WebUI.closeBrowser()

