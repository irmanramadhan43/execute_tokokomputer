import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('2_Supplier_OR/Page_Dashboard/a_DATA MASTER'), GlobalVariable.delayTime, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Dashboard/a_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Dashboard/span_Data Supplier'))

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Dashboard/a_Tambah Supplier'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Form Supplier/input_Nama Supplier_nama_supp'), 'Purnama', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Form Supplier/input_No Telp_no_supp'), '099', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Form Supplier/input_Alamat_alamat_supp'), 'Bandung', 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Form Supplier/button_Simpan'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delayTime)

WebUI.verifyElementText(findTestObject('2_Supplier_OR/Page_Data Supplier/dataBerhasildiBuat'), 'Data Berhasil dibuat')

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Data Supplier/button_Edit'))

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Edit Supplier/input_Nama Supplier_nama_supp'), 'Purnama_1')

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Edit Supplier/input_No Telp_no_supp'), '0887899800')

WebUI.setText(findTestObject('Object Repository/2_Supplier_OR/Page_Edit Supplier/input_Alamat_alamat_supp'), 'Bandung, BTM')

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Edit Supplier/button_Simpan'))

WebUI.delay(GlobalVariable.delayTime)

WebUI.click(findTestObject('Object Repository/2_Supplier_OR/Page_Data Supplier/button_Hapus'))

WebUI.delay(GlobalVariable.delayTime)

WebUI.setText(findTestObject('2_Supplier_OR/Page_Data Supplier/searchDataSuplier'), 'NJT', FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delayTime)

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CallTestCase/TC-closeBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

