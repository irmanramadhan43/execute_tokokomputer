import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delayTime)

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Dashboard/a_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Dashboard/span_Data SuperSubKategori'))

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Dashboard/a_Tambah SuperSubKategori'))

WebUI.selectOptionByValue(findTestObject('Object Repository/5_SuperSubKategori/Page_Tambah Data SuperSubKategori/select_- Pilih Kategori -ProsesorRamVgaStro_fa3120'), 
    '6', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/5_SuperSubKategori/Page_Tambah Data SuperSubKategori/select_Ssd Nvme'), 
    '32', true)

WebUI.setText(findTestObject('Object Repository/5_SuperSubKategori/Page_Tambah Data SuperSubKategori/input_Judul SuperSubKategori_judul_supersub_ac260e'), 
    'Transcend')

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Tambah Data SuperSubKategori/button_Simpan'))

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Data SuperSubKategori/i_Aksi_fa fa-pencil'))

WebUI.setText(findTestObject('Object Repository/5_SuperSubKategori/Page_Ubah Data SuperSubKategori/input_Judul SuperSubKategori_judul_supersub_ac260e'), 
    'Transcend_1')

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Ubah Data SuperSubKategori/button_Simpan'))

WebUI.click(findTestObject('5_SuperSubKategori/Page_Data SuperSubKategori/hapus_SuperSubKategori'))

WebUI.waitForAlert(GlobalVariable.delayTime)

WebUI.delay(GlobalVariable.delayTime)

WebUI.acceptAlert()

WebUI.setText(findTestObject('Object Repository/5_SuperSubKategori/Page_Data SuperSubKategori/input_Search_form-control input-sm'), 
    'MSI')

WebUI.delay(GlobalVariable.delayTime)

WebUI.click(findTestObject('Object Repository/5_SuperSubKategori/Page_Data SuperSubKategori/a_Refresh'))

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CallTestCase/TC-closeBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

