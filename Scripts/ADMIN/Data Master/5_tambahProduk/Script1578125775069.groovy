import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/span_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/span_Data Produk'))

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Dashboard/a_Tambah Produk'))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Nama Produk_judul_produk'), 
    findTestData('ExcelDataSource/Data Produk Prosesor').getValue(1, 1))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Keywords_keywords'), findTestData(
        'ExcelDataSource/Data Produk Prosesor').getValue(2, 1))

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/p'))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/body_8 core 16 thread'), findTestData(
        'ExcelDataSource/Data Produk Prosesor').getValue(3, 1))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Harga Normal_harga_normal'), 
    findTestData('ExcelDataSource/Data Produk Prosesor').getValue(4, 1))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Diskon ()_diskon'), findTestData(
        'ExcelDataSource/Data Produk Prosesor').getValue(5, 1))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Stok_stok'), findTestData('ExcelDataSource/Data Produk Prosesor').getValue(
        6, 1))

WebUI.setText(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/input_Berat_berat'), findTestData(
        'ExcelDataSource/Data Produk Prosesor').getValue(7, 1))

WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_- Pilih Kategori -ProsesorRamVgaStro_643f17'), 
    '3', true)

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/div_Stok'))

WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_- Pilih Kategori -ProsesorRamVgaStro_643f17'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/select_AmdI9 SeriesIntel'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('6_tambahProduk/Page_Tambah Data Produk/select_I9 Series'), '12', true)

WebUI.uploadFile(findTestObject('6_tambahProduk/Page_Dashboard/uploadGambar'), 'C:\\Users\\User\\Pictures\\426f99bfecf0f226ee043a719a4e6f4f.jpg')

WebUI.click(findTestObject('Object Repository/6_tambahProduk/Page_Tambah Data Produk/button_Simpan'))

WebUI.click(findTestObject('6_tambahProduk/Page_Data Produk/i_Lihat Gambar_fa fa-pencil'))

WebUI.switchToWindowTitle('Ubah Data Produk')

WebUI.setText(findTestObject('6_tambahProduk/Page_Ubah Data Produk/input_Harga Normal_harga_normal'), '7000000')

WebUI.click(findTestObject('6_tambahProduk/Page_Ubah Data Produk/button_Simpan'))

WebUI.switchToWindowTitle('Data Produk')

WebUI.click(findTestObject('6_tambahProduk/Page_Data Produk/a_Lihat Gambar'))

WebUI.switchToWindowTitle('Data Produk')

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CallTestCase/TC-closeBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

