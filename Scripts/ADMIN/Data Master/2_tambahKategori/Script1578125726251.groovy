import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(GlobalVariable.delayTime)

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Dashboard/a_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Dashboard/span_Data Kategori'))

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Dashboard/a_Tambah Kategori'))

WebUI.setText(findTestObject('Object Repository/3_Kategori_OR/Page_Tambah Data Kategori/input_Judul Kategori_judul_kategori'), 
    'Periferal')

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Tambah Data Kategori/button_Simpan'))

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Data Kategori/a_Aksi_btn btn-sm btn-warning'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/3_Kategori_OR/Page_Ubah Data Kategori/input_Judul Kategori_judul_kategori'), 
    'Periferal_1')

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Ubah Data Kategori/button_Simpan'))

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Data Kategori/a_Aksi_btn btn-sm btn-danger_1'))

WebUI.waitForAlert(GlobalVariable.delayTime)

WebUI.delay(GlobalVariable.delayTime)

WebUI.acceptAlert()

WebUI.setText(findTestObject('Object Repository/3_Kategori_OR/Page_Data Kategori/input_Search_form-control input-sm'), 'periferal')

WebUI.delay(GlobalVariable.delayTime)

WebUI.click(findTestObject('Object Repository/3_Kategori_OR/Page_Data Kategori/a_Refresh'))

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CallTestCase/TC-closeBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

