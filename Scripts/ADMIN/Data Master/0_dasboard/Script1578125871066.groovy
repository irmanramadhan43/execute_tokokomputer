import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.openBrowser('http://localhost/kl_skripsi/admin/auth/login')

WebUI.callTestCase(findTestCase('CallTestCase/TC-Login'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.waitForElementVisible(findTestObject('1_Dasboard_OR/b_FS KOMPUTER'), 10)

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/b_FS KOMPUTER'), FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/a_Toggle navigation'))

WebUI.click(findTestObject('1_Dasboard_OR/b_FS KOMPUTER'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Halo admin'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/img_Toggle navigation_user-image'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/a_Selengkapnya'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Dashboard'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/a_Selengkapnya_1'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Dashboard (1)'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/a_Selengkapnya_1_2'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Dashboard (2)'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/button_Lihat Selengkapnya'))

WebUI.click(findTestObject('Object Repository/1_Dasboard_OR/span_Dashboard'))

WebUI.callTestCase(findTestCase('CallTestCase/TC-Logout'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('CallTestCase/TC-closeBrowser'), [:], FailureHandling.STOP_ON_FAILURE)

