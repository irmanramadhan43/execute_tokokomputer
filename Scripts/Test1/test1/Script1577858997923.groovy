import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.Url)

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_email'), GlobalVariable.User)

WebUI.setEncryptedText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_password'), 'vRRrCeg9Qho=')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Sign In'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_email'), 'admin')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_password'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Sign In'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_email'), 'admin@gmail.com')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Admin_password'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Sign In'))

WebUI.waitForElementVisible(findTestObject('DataMaster_OR1/a_DATA MASTER'), 10)

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_DATA MASTER'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Data Supplier'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Tambah Supplier'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Nama Supplier_nama_supp'), 'NJT')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_No Telp_no_supp'), '007')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/input_Alamat_alamat_supp'))

WebUI.doubleClick(findTestObject('Object Repository/DataMaster_OR1/input_Alamat_alamat_supp'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Alamat_alamat_supp'), 'Bandung')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Simpan'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_DATA MASTER (1)'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/span_Data Kategori'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Tambah Kategori'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Judul Kategori_judul_kategori'), 'Casing')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Simpan (1)'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Data Kategori'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/span_Data SubKategori'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Tambah SubKategori'))

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_- Pilih Kategori -ProsesorRamVgaStro_feae27'), 
    '3', true)

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Judul SubKategori_judul_subkategori'), 'Intel')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Simpan (2)'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/span_Data SuperSubKategori'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Tambah SuperSubKategori'))

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_- Pilih Kategori -ProsesorRamVgaStro_feae27 (1)'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_AmdI9 SeriesIntelIntel'), '8', true)

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Judul SuperSubKategori_judul_supersub_ac260e'), 'i7 series')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/button_Simpan (3)'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/span_Data Produk'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/a_Tambah Produk'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Nama Produk_judul_produk'), 'I7 9876')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Keywords_keywords'), 'gaming')

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/p'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/html_mce-content-body divmce-resizehandle p_4d1aed'))

WebUI.click(findTestObject('Object Repository/DataMaster_OR1/p_8 core 16 thread'))

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/body_8 core 16 thread'), '8 core 16 thread')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Harga Normal_harga_normal'), '780000')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Diskon ()_diskon'), '10')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Stok_stok'), '10')

WebUI.setText(findTestObject('Object Repository/DataMaster_OR1/input_Berat_berat'), '2')

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_- Pilih Kategori -ProsesorRamVgaStro_feae27 (2)'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_AmdI9 SeriesIntelIntel (1)'), '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataMaster_OR1/select_I9 SeriesI7 Series'), '20', true)

WebUI.uploadFile(findTestObject('dataProduk_OR/inputGambar'), 'C:\\Users\\User\\Pictures\\windows-black-wallpaper_2047090.jpg')

WebUI.click(findTestObject('DataMaster_OR1/button_Simpan (4)'))

WebUI.waitForElementClickable(findTestObject('DataPembelian_OR1/a_DATA PEMBELIAN'), 5)

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_DATA PEMBELIAN'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_Form Pemesanan'))

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Techno Magic                        _1f45a1'), 
    'SP2', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _83bb19'), 
    '5', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _2d670d'), 
    '10', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _225918'), 
    '14', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _225918'), 
    '17', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _83bb19'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _4f01f0'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _342ad2'), 
    '20', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _73b58a'), 
    'I7 9876', true)

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/input_Nama  Barang (Jika Tidak ada ketik ba_52ea68'))

WebUI.setText(findTestObject('Object Repository/DataPembelian_OR1/input_Jumlah_jumlah'), '2')

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/button_Simpan (5)'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/a_Selesai'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_DATA PEMBELIAN (1)'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_Data Pemesanan'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_Form Penerimaan'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_Form Pemesanan (1)'))

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _83bb19'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _4f01f0'), 
    '8', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _342ad2'), 
    '12', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _9800f9'), 
    'Intel Core i7-9700K', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _9800f9'), 
    'INTEL Processor Core i9-4150', true)

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/input_Nama  Barang (Jika Tidak ada ketik ba_52ea68'))

WebUI.setText(findTestObject('Object Repository/DataPembelian_OR1/textarea_Deskripsi Barang_deskripsi'), '12 core 24 thread')

WebUI.setText(findTestObject('Object Repository/DataPembelian_OR1/input_Jumlah_jumlah'), '14')

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/button_Simpan (5)'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/a_Tambah Barang'))

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _83bb19 (1)'), 
    '3', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _4f01f0 (1)'), 
    '9', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _6b7d8f'), 
    '16', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/DataPembelian_OR1/select_Please Select                       _44e54c'), 
    'AMD - Ryzen 7 2700X', true)

WebUI.setText(findTestObject('Object Repository/DataPembelian_OR1/textarea_Deskripsi Barang_deskripsi (1)'), '8 core 16 thread')

WebUI.setText(findTestObject('Object Repository/DataPembelian_OR1/input_Jumlah_jumlah (1)'), '12')

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/button_Tambah'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/a_Selesai'))

String path = WebUI.takeScreenshot()

WebUI.takeScreenshot('c:\\Documents\\test.png')

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/th_Tanggal Order'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/th_Tanggal Order'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/span_Halo admin'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/a_DATA PEMBELIAN'))

WebUI.click(findTestObject('Object Repository/DataPembelian_OR1/a_Form Penerimaan'))

WebUI.closeBrowser()

