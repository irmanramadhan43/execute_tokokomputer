import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://flutter.github.io/samples')

WebUI.waitForPageLoad(40)

WebUI.delay(40)

WebUI.navigateToUrl('https://flutter.github.io/samples/#/login', FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Flutter_OR/input_user_name'))

WebUI.setText(findTestObject('Flutter_OR/input_user_name'), 'Tony')

WebUI.click(findTestObject('Flutter_OR/body_glass_pane'))

WebUI.click(findTestObject('Flutter_OR/input_password'))

WebUI.setText(findTestObject('Flutter_OR/input_password'), '123456')

WebUI.delay(10)

WebUI.closeBrowser()

